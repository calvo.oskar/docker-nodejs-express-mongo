Boilerplate para desarrollos de nodejs + express + mongo con Docker

He usado como base para montarlo el curso de Mozilla.org
 https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/development_environment 
 y https://github.com/mdn/express-locallibrary-tutorial .
Además el docker lo he montado basándome en 
https://appdividend.com/2018/04/13/how-to-setup-node-express-and-mongodb-in-docker/#Setup_Node_Express_and_MongoDB_in_Docker
